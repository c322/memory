#include <stdlib.h>
#include <string.h>
#include "port.h"

static void *bctbx_libc_malloc(size_t sz)
{
    return malloc(sz);
}

static void *bctbx_libc_realloc(void *ptr, size_t sz)
{
    return realloc(ptr,sz);
}

static void bctbx_libc_free(void*ptr)
{
    free(ptr);
}

static bool_t allocator_used = FALSE;

static BctoolboxMemoryFunctions bctbx_allocator = {
    bctbx_libc_malloc,
    bctbx_libc_realloc,
    bctbx_libc_free
};

void bctbx_set_memory_functions(BctoolboxMemoryFunctions *functions)
{
    if (allocator_used)
    {
        printf("bctbx_set_memory_functions() must be called before "
               "first use of bctbx_malloc or bctbx_realloc\n");
        return;
    }
    bctbx_allocator = *functions;
}

void* bctbx_malloc(size_t sz)
{
    allocator_used = TRUE;
    return bctbx_allocator.malloc_fun(sz);
}

void* bctbx_realloc(void *ptr, size_t sz)
{
    allocator_used = TRUE;
    return bctbx_allocator.realloc_fun(ptr,sz);
}

void bctbx_free(void* ptr)
{
    bctbx_allocator.free_fun(ptr);
}

void * bctbx_malloc0(size_t size)
{
    void *ptr = bctbx_malloc(size);
    if (ptr)    memset(ptr, 0, size);
    return ptr;
}


