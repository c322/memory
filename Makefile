SRC := ./port.c
INC := ./
SHARED := libmemory.so
CC  := gcc

all:
	${CC} ${SRC} -o ${SHARED} -fPIC -shared 
clean:
	rm ${SHARED}
