#ifndef ORTP_PORT_H
#define ORTP_PORT_H
#include <stdio.h>

#if defined(_WIN32) || defined(_WIN32_WCE)      /*windows*/
#ifdef BCTBX_STATIC                             /*static*/
#define BCTBX_PUBLIC
#else                                           /*shared*/
#ifdef BCTBX_EXPORTS                            /*export*/
#define BCTBX_PUBLIC    __declspec(dllexport)
#else                                           /*import*/
#define BCTBX_PUBLIC    __declspec(dllimport)
#endif
#endif
#else                                           /*linux*/
#define BCTBX_PUBLIC
#endif

#define bool_t int
#define TRUE  1
#define FALSE 0

#ifdef __cplusplus
extern "C"{
#endif

BCTBX_PUBLIC void* bctbx_malloc(size_t sz);
BCTBX_PUBLIC void bctbx_free(void *ptr);
BCTBX_PUBLIC void* bctbx_realloc(void *ptr, size_t sz);
BCTBX_PUBLIC void* bctbx_malloc0(size_t sz);

/*override the allocator with this method, to be called BEFORE bctbx_init()*/
typedef struct _BctoolboxMemoryFunctions
{
    void *(*malloc_fun)(size_t sz);
    void *(*realloc_fun)(void *ptr, size_t sz);
    void (*free_fun)(void *ptr);
}BctoolboxMemoryFunctions;

BCTBX_PUBLIC void bctbx_set_memory_functions(BctoolboxMemoryFunctions *functions);

#define bctbx_new(type,count)   (type*)bctbx_malloc(sizeof(type)*(count))
#define bctbx_new0(type,count)  (type*)bctbx_malloc0(sizeof(type)*(count))


#ifdef __cplusplus
}
#endif

#endif
